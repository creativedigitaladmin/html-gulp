var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var uglify = require('gulp-uglifyjs');
var gulpIf = require('gulp-if');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var useref = require('gulp-useref');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');

//test task
gulp.task('hello', function() {
  console.log('Hello Loyiso');
});
// gulp tasks run here

//gulp sass to css
gulp.task('sass', function(){
  return gulp.src('./sass/**/*.scss')
    .pipe(sass()) // Converts Sass to CSS with gulp-sass
    //.pipe(sourcemaps.init())
     .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
     .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 7', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
     .pipe(sourcemaps.write('/'))
    .pipe(gulp.dest('./css'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

//uglify js
gulp.task('uglify', function() {
  gulp.src('./lib/*.js')
    .pipe(uglify('main.min.js'))
    .pipe(gulp.dest('./js'))
    .pipe(browserSync.reload({
      stream: true
      }))
});

//imagemin
gulp.task('images', function(){
  return gulp.src('./images/*.+(png|jpg|gif|svg)')
  .pipe(imagemin())
  .pipe(gulp.dest('./imagesminified/'))
});


gulp.task('useref', function(){
  return gulp.src('./*.html')
    .pipe(useref())
    // Minifies only if it's a JavaScript file
    .pipe(gulpIf('*.js', uglify()))
    // Minifies only if it's a CSS file
    .pipe(gulpIf('*.css', cssnano()))
    .pipe(gulp.dest('dist'))
});

gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: './'
    },
  })
})


//watchers
//sass watcher
gulp.task('watch', ['browserSync', 'sass', 'uglify'], function(){
  gulp.watch('./sass/**/*.scss', ['sass']); 
  gulp.watch('./*.html', browserSync.reload);
  gulp.watch('./lib/*.js', ['uglify'], browserSync.reload);
  // Other watchers
})